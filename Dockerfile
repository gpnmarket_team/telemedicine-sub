FROM artifactory.gazprom-neft.local/ci-docker-local/dev-base/rhel7-gpn

COPY ./addUserAndGroup.sh ./

RUN chmod +x ./addUserAndGroup.sh && ./addUserAndGroup.sh && rm -f ./addUserAndGroup.sh

ENV APP /opt/app

RUN mkdir -p $APP

WORKDIR $APP

COPY ./bin $APP/

EXPOSE 9000

RUN chown -R 1001020000:1500 $APP && chmod +x sub_client

USER 1001020000

ENTRYPOINT ./sub_client \
    -rootCa /run/secrets/ca.pem \
    -clientCa /run/secrets/client.pem \
    -clientKey /run/secrets/client.key \
    -server $NATS_HOST \
    -worker "${BACKEND_HOST}:${BACKEND_PORT}" \
    -uName $SUB_USER -uPass $SUB_PASS \
    -debug true



