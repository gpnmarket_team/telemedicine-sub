package main

import (
	"bufio"
	"crypto/tls"
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"net/http/httputil"
	"runtime"
	"strings"
	"time"

	"github.com/nats-io/go-nats"
)

func usage() {
	log.Printf("Usage: nats-rply [-s server] [-creds file] [-t] <subject> <response>\n")
	flag.PrintDefaults()
}

var debug = false

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	ca := flag.String("rootCa", "", "ca")
	cert := flag.String("clientCa", "", "client cert")
	key := flag.String("clientKey", "", "client key")
	server := flag.String("server", "tls://127.0.0.1:4222", "server url")
	user := flag.String("uName", "sub_client", "user name")
	pass := flag.String("uPass", "", "user password")

	worker := flag.String("worker", "127.0.0.1", "worker url")
	debugMode := flag.Bool("debug", false, "debug mode")
	subj := "promocode"

	flag.Parse()
	debug = *debugMode
	if *ca == "" || *cert == "" || *key == "" {
		log.Fatal("need certificate path", *ca, *cert, *key)
	}
	if server != nil || worker != nil {
		log.Printf("%s", *server)
	}
	log.Printf("Client sub [%s] start \n", subj)

	time.Sleep(2 * time.Second)
	testWorker, errTest := http.Get("http://" + *worker)
	if testWorker != nil && testWorker.StatusCode == http.StatusOK {
		log.Printf("Worker [%s] OK \n", *worker)
	} else if testWorker != nil {
		log.Printf("Worker [%s] status [%s] \n", *worker, testWorker.StatusCode)
	} else {
		log.Printf("Worker [%s] failed (%v) \n", *worker, errTest)
	}

	opts := []nats.Option{nats.Name("NATS Sample Responder"), nats.RootCAs(*ca), nats.ClientCert(*cert, *key), nats.UserInfo(*user, *pass)}
	opts = setupConnOptions(opts)
	// Connect to NATS
	nc, err := nats.Connect(*server, opts...)
	if err != nil {
		log.Fatal(err)
	}

	nc.Subscribe(subj, func(msg *nats.Msg) {
		nc.Publish(msg.Reply, backReq(msg, *worker))
	})
	nc.Flush()

	if err := nc.LastError(); err != nil {
		log.Fatal(err)
	}

	runtime.Goexit()
}

func setupConnOptions(opts []nats.Option) []nats.Option {
	totalWait := 10 * time.Minute
	reconnectDelay := time.Second

	opts = append(opts, nats.ReconnectWait(reconnectDelay))
	opts = append(opts, nats.MaxReconnects(int(totalWait/reconnectDelay)))
	opts = append(opts, nats.DisconnectHandler(func(nc *nats.Conn) {
		log.Printf("Disconnected: will attempt reconnects for %.0fm", totalWait.Minutes())
	}))
	opts = append(opts, nats.ReconnectHandler(func(nc *nats.Conn) {
		log.Printf("Reconnected [%s]", nc.ConnectedUrl())
	}))
	opts = append(opts, nats.ClosedHandler(func(nc *nats.Conn) {
		log.Fatal("Exiting, no servers available")
	}))
	return opts
}

func backReq(m *nats.Msg, host string) []byte {
	log.Print("Beep")
	var msgDecode map[string]string
	err := json.Unmarshal(m.Data, &msgDecode)
	if err != nil {
		return []byte(err.Error())
	}

	b := bufio.NewReader(strings.NewReader(msgDecode["request"]))

	req, err := http.ReadRequest(b)

	req.URL.Host = host
	req.Host = ""
	req.URL.Scheme = "https"
	req.RequestURI = ""

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{
		Transport: tr,
	    CheckRedirect: func(req *http.Request, via []*http.Request) error {
        	return http.ErrUseLastResponse
		},
	}

	resp, err := client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		log.Printf("%s", err)
	}
	if resp == nil {
		resp = &http.Response{
			StatusCode: http.StatusServiceUnavailable,
		}
	}
	respB, _ := httputil.DumpResponse(resp, true)
	log.Print("Boop")

	if debug {
		log.Printf("%s", string(respB))
	}

	return respB
}
