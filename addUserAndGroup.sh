#!/bin/bash

GROUP_ID=1500

groupadd -g ${GROUP_ID} app-users

for USER_ID in 1001020000 1000689999 1000949999; do
	useradd -u ${USER_ID} -g ${GROUP_ID} -G ${GROUP_ID} app-user${USER_ID} --no-log-init
done	
